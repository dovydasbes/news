<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\Validation\UserValidatorService;
use Illuminate\Console\Command;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Validation\ValidationException;

class CreateUser extends Command
{
    /**
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * @var string
     */
    protected $description = 'Creates user';

    /**
     * @var UserValidatorService
     */
    private $validator;

    /**
     * @var Hasher
     */
    private $hasher;

    /**
     * @var User
     */
    private $user;

    /**
     * @param UserValidatorService $validator
     * @param Hasher $hasher
     * @param User $user
     */
    public function __construct(UserValidatorService $validator, Hasher $hasher, User $user)
    {
        parent::__construct();

        $this->validator = $validator;
        $this->hasher = $hasher;
        $this->user = $user;
    }

    /**
     * @throws ValidationException
     */
    public function handle(): void
    {
        $name = $this->ask('Enter your name');
        $email = $this->ask('Enter your email');
        $password = $this->secret('Enter your password (it will be hidden)');
        $repeatedPassword = $this->secret('Repeat password');

        try {
            $this->validator->validateUser([
                'name' => $name,
                'email' => $email,
                'password' => $password,
                'password_confirmation' => $repeatedPassword,
            ]);

            $user = $this->user->newInstance([
                'name' => $name,
                'email' => $email,
                'password' => $this->hasher->make($password),
            ]);
            $user->saveOrFail();

            $this->info('Your account has beeen created!');
        } catch (ValidationException $e) {
            $this->error(collect($e->errors())->collapse()->implode(' '));
        }
    }
}
