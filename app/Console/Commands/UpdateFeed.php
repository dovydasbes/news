<?php

namespace App\Console\Commands;

use App\Repositories\ProviderRepository;
use App\Services\Import\Exception\ImportException;
use App\Services\Import\Rss\RssImporterService;
use Illuminate\Console\Command;

class UpdateFeed extends Command
{
    /**
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * @var string
     */
    protected $description = 'Updates feeds from rss';

    /**
     * @var ProviderRepository
     */
    private $providerRepo;

    /**
     * @var RssImporterService
     */
    private $rss;

    public function __construct(ProviderRepository $providerRepo, RssImporterService $rss)
    {
        parent::__construct();

        $this->providerRepo = $providerRepo;
        $this->rss = $rss;
    }

    public function handle(): void
    {
        $this->info('Feed update starting!');

        $providers = $this->providerRepo->findEnabled();
        $count = $providers->count();

        foreach ($providers as $key => $value) {
            $number = ++$key;
            $this->info("$number/$count Updating provider: {$value->name} ");

            try {
                $this->rss->import($value);
            } catch (ImportException $e) {
                $this->error('Skipping! ' . $e->getMessage());
            }
        }

        if ($count === 0) {
            $this->info('No providers found, aborting!');
        } else {
            $this->info('Feed successfully updated!');
        }
    }
}
