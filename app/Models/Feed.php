<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string $description
 * @property string $guid
 * @property Carbon $published
 * @property Provider $provider
 */
class Feed extends Model
{
    const HOME_LIMIT = 50;

    /**
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'description', 'provider_id', 'guid', 'published',
    ];

    /**
     * @return BelongsTo
     */
    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class, 'provider_id');
    }
}
