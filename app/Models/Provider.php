<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $url
 * @property string $rss
 * @property bool $enabled
 * @property Collection $categories
 */
class Provider extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'logo', 'url', 'rss', 'enabled',
    ];

    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'provider_categories');
    }
}
