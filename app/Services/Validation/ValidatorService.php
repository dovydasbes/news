<?php namespace App\Services\Validation;

use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

class ValidatorService
{
    /**
     * @var Factory
     */
    private $validator;

    /**
     * @param Factory $validator
     */
    function __construct(Factory $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param array $attributes
     * @param array $rules
     * @throws ValidationException
     */
    protected function validate(array $attributes, array $rules): void
    {
        $validator = $this->validator->make($attributes, $rules);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }
}
