<?php namespace App\Services\Validation;

use App\Models\User;
use Illuminate\Validation\ValidationException;

class UserValidatorService extends ValidatorService
{
    /**
     * @param array $attributes
     * @throws ValidationException
     */
    public function validateUser(array $attributes): void
    {
        $this->validate($attributes, [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|string|min:' . User::MIN_PASSWORD_LENGTH,
        ]);
    }
}
