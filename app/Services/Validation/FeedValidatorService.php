<?php namespace App\Services\Validation;

use Illuminate\Validation\ValidationException;

class FeedValidatorService extends ValidatorService
{
    /**
     * @param array $attributes
     * @throws ValidationException
     */
    public function validateFeed(array $attributes): void
    {
        $this->validate($attributes, [
            'title' => 'required|string|max:255',
            'url' => 'required|url',
            'description' => 'sometimes',
            'published' => 'required|date',
            'guid' => 'required|string',
        ]);
    }
}
