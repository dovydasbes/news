<?php namespace App\Services\User;

use App\Models\User;
use Illuminate\Auth\AuthManager;
use Illuminate\Config\Repository;

class LoginService
{
    /**
     * @var \Illuminate\Contracts\Auth\StatefulGuard
     */
    private $auth;

    /**
     * @var Repository
     */
    private $config;

    /**
     * @param AuthManager $auth
     * @param Repository $config
     */
    public function __construct(AuthManager $auth, Repository $config)
    {
        $this->auth = $auth->guard();
        $this->config = $config;
    }

    /**
     * @param User $user
     */
    public function loginUser(User $user): void
    {
        $this->auth->login($user);
    }

    public function logout(): void
    {
        $this->auth->logout();
    }

    /**
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials): bool
    {
        return $this->auth->validate($credentials);
    }
}
