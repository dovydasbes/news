<?php namespace App\Services\Category;

use Illuminate\Support\Collection;

class ParameterGeneratorService
{
    public function generate(Collection $selectedCategories, int $id)
    {
        $categories = clone $selectedCategories;

        $key = $categories->search($id);

        if ($key !== false) {
            return $categories->forget($key)->toArray();
        }

        return $categories->push($id)->toArray();
    }
}
