<?php namespace App\Services\Import\Exception;

/**
 * Class ImportException
 */
class ImportException extends \Exception
{
    /**
     * @return ImportException
     */
    public static function corruptedXML(): ImportException
    {
        return new self("XML is corrupted");
    }

    /**
     * @return ImportException
     */
    public static function invalidXML(): ImportException
    {
        return new self("XML format is invalid");
    }

    /**
     * @return ImportException
     */
    public static function invalidFeed(): ImportException
    {
        return new self("XML has invalid feed items");
    }
}
