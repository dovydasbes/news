<?php namespace App\Services\Import\Rss;

use App\Models\Feed;
use App\Models\Provider;
use App\Repositories\FeedRepository;
use App\Services\Import\Exception\ImportException;
use App\Services\Validation\FeedValidatorService;
use Carbon\Carbon;
use SimpleXMLElement;

class RssImporterService
{
    const TYPE_CHANNEL = 'channel';

    /**
     * @var Feed
     */
    private $feed;

    /**
     * @var FeedRepository
     */
    private $feedRepo;

    /**
     * @var FeedValidatorService
     */
    private $validator;

    /**
     * @param Feed $feed
     * @param FeedRepository $feedRepo
     * @param FeedValidatorService $validator
     */
    function __construct(Feed $feed, FeedRepository $feedRepo, FeedValidatorService $validator)
    {
        $this->feed = $feed;
        $this->feedRepo = $feedRepo;
        $this->validator = $validator;
    }

    /**
     * @param Provider $provider
     * @throws ImportException
     */
    public function import(Provider $provider): void
    {
        try {
            $xml = new SimpleXMLElement(file_get_contents($provider->rss));
        } catch (\Exception $e) {
            throw ImportException::corruptedXML();
        }

        $channel = $xml->channel;

        if (!$channel instanceof SimpleXMLElement || $channel->getName() !== self::TYPE_CHANNEL) {
            throw ImportException::invalidXML();
        }

        \DB::beginTransaction();
        try {
            foreach ($channel->item as $item) {
                $guid = (string)$item->guid;

                $attributes = [
                    'title' => (string)$item->title,
                    'description' => (string)$item->description,
                    'url' => (string)$item->link,
                    'guid' => $guid,
                    'published' => Carbon::parse((string)$item->pubDate)->setTimezone(config('app.timezone')),
                ];

                $this->validator->validateFeed($attributes);

                $feed = $this->feedRepo->findByProviderAndGuid($provider, $guid);

                if ($feed) {
                    $feed->fill($attributes);
                } else {
                    $feed = $this->feed->newInstance($attributes);

                    $feed->provider()->associate($provider);
                }

                $feed->saveOrFail();
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();

            throw ImportException::invalidFeed();
        }
    }
}
