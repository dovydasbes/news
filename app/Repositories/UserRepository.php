<?php namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserRepository
 */
class UserRepository extends BaseRepository
{
    /**
     * @param string $email
     * @return User
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findByEmail(string $email): User
    {
        return $this->query()->where('email', $email)->firstOrFail();
    }

    /**
     * @return Builder
     */
    protected function query(): Builder
    {
        return User::query();
    }
}
