<?php namespace App\Repositories;

use App\Models\Feed;
use App\Models\Provider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class FeedRepository
 */
class FeedRepository extends BaseRepository
{
    /**
     * @param Provider $provider
     * @param string $guid
     * @return Feed|null
     */
    public function findByProviderAndGuid(Provider $provider, string $guid): ?Feed
    {
        return $this->query()->where('provider_id', $provider->id)->where('guid', $guid)->first();
    }

    /**
     * @param \Illuminate\Support\Collection $categories
     * @return Collection
     */
    public function findByCategories(\Illuminate\Support\Collection $categories): Collection
    {
        $query = $this->query();

        if ($categories->isNotEmpty()) {
            $query->leftJoin('provider_categories', 'provider_categories.provider_id', '=', 'feeds.provider_id')
                ->whereIn('provider_categories.category_id', $categories)
                ->groupBy('feeds.id');
        }

        return $query->orderBy('published', 'desc')->limit(Feed::HOME_LIMIT)->get();
    }

    /**
     * @return Builder
     */
    protected function query(): Builder
    {
        return Feed::query();
    }
}
