<?php namespace App\Repositories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CategoryRepository
 */
class CategoryRepository extends BaseRepository
{
    /**
     * @return Collection
     */
    public function findCategoriesOrderedByName(): Collection
    {
        return $this->query()->orderBy('name')->get();
    }

    /**
     * @return Builder
     */
    protected function query(): Builder
    {
        return Category::query();
    }
}
