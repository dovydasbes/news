<?php namespace App\Repositories;

use App\Models\Provider;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ProviderRepository
 */
class ProviderRepository extends BaseRepository
{
    /**
     * @return Collection
     */
    public function findAllOrderedByName(): Collection
    {
        return $this->query()->orderBy('name')->get();
    }

    /**
     * @return Collection
     */
    public function findEnabled(): Collection
    {
        return $this->query()->where('enabled', true)->get();
    }

    /**
     * @return Builder
     */
    protected function query(): Builder
    {
        return Provider::query();
    }
}
