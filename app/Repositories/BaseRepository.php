<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 */
abstract class BaseRepository
{
    abstract protected function query(): Builder;

    /**
     * @param string $id
     * @return Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findById(string $id): Model
    {
        return $this->query()->where('id', $id)->firstOrFail();
    }
}
