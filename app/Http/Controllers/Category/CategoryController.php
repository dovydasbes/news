<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryCreateRequest;
use App\Http\Requests\Category\CategoryUpdateRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepo;

    function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * @return View
     */
    public function showList(): View
    {
        return view('category.list', [
            'categories' => $this->categoryRepo->findCategoriesOrderedByName(),
        ]);
    }

    /**
     * @return View
     */
    public function showCreate(): View
    {
        return $this->update(new Category());
    }

    /**
     * @param CategoryCreateRequest $request
     * @return RedirectResponse
     */
    public function submitCreate(CategoryCreateRequest $request): RedirectResponse
    {
        $category = new Category($request->validated());
        $category->saveOrFail();

        return redirect()->route('category:list')->with('success', 'Successfully created category!');
    }

    /**
     * @param Category $category
     * @return View
     */
    public function showUpdate(Category $category): View
    {
        return $this->update($category);
    }

    /**
     * @param Category $category
     * @param CategoryUpdateRequest $request
     * @return RedirectResponse
     */
    public function submitUpdate(Category $category, CategoryUpdateRequest $request): RedirectResponse
    {
        $category->fill($request->validated());
        $category->saveOrFail();

        return redirect()->route('category:update', $category)->with('success', 'Successfully updated category!');
    }

    /**
     * @param Category $category
     * @return View
     */
    private function update(Category $category): View
    {
        return view('category.form', [
            'category' => $category,
        ]);
    }
}
