<?php

namespace App\Http\Controllers\Provider;

use App\Http\Controllers\Controller;
use App\Http\Requests\Provider\ProviderRequest;
use App\Models\Provider;
use App\Repositories\CategoryRepository;
use App\Repositories\ProviderRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ProviderController extends Controller
{
    /**
     * @var ProviderRepository
     */
    private $providerRepo;

    /**
     * @var CategoryRepository
     */
    private $categoryRepo;

    /**
     * @param ProviderRepository $providerRepo
     * @param CategoryRepository $categoryRepo
     */
    function __construct(ProviderRepository $providerRepo, CategoryRepository $categoryRepo)
    {
        $this->providerRepo = $providerRepo;
        $this->categoryRepo = $categoryRepo;
    }

    /**
     * @return View
     */
    public function showList(): View
    {
        return view('provider.list', [
            'providers' => $this->providerRepo->findAllOrderedByName(),
        ]);
    }

    /**
     * @return View
     */
    public function showCreate(): View
    {
        return $this->update(new Provider());
    }

    /**
     * @param ProviderRequest $request
     * @return RedirectResponse
     */
    public function submitCreate(ProviderRequest $request): RedirectResponse
    {
        $provider = new Provider($request->validated());
        $provider->saveOrFail();

        $provider->categories()->sync($request->get('category'));

        return redirect()->route('provider:list')->with('success', 'Successfully created provider!');
    }

    /**
     * @param Provider $provider
     * @return View
     */
    public function showUpdate(Provider $provider): View
    {
        return $this->update($provider);
    }

    /**
     * @param Provider $provider
     * @param ProviderRequest $request
     * @return RedirectResponse
     */
    public function submitUpdate(Provider $provider, ProviderRequest $request): RedirectResponse
    {
        $provider->fill($request->validated());
        $provider->saveOrFail();

        $provider->categories()->sync($request->get('category'));

        return redirect()->back()->with('success', 'Successfully updated provider!');
    }

    /**
     * @param Provider $provider
     * @return RedirectResponse
     */
    public function submitDelete(Provider $provider): RedirectResponse
    {
        $provider->delete();

        return redirect()->back()->with('success', 'Successfully deleted provider!');
    }

    /**
     * @param Provider $provider
     * @return View
     */
    private function update(Provider $provider): View
    {
        return view('provider.form', [
            'provider' => $provider,
            'categories' => $this->categoryRepo->findCategoriesOrderedByName(),
            'existingCategories' => $provider->categories()->get()->pluck('id'),
        ]);
    }
}
