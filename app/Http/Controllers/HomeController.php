<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use App\Repositories\FeedRepository;
use App\Services\Category\ParameterGeneratorService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * @var FeedRepository
     */
    private $feedRepo;

    /**
     * @var CategoryRepository
     */
    private $categoryRepo;

    /**
     * @var ParameterGeneratorService
     */
    private $generator;

    /**
     * @param FeedRepository $feedRepo
     * @param CategoryRepository $categoryRepo
     */
    function __construct(FeedRepository $feedRepo, CategoryRepository $categoryRepo, ParameterGeneratorService $generator)
    {
        $this->feedRepo = $feedRepo;
        $this->categoryRepo = $categoryRepo;
        $this->generator = $generator;
    }

    /**
     * @return View
     */
    public function showHome(Request $request): View
    {
        $categories = collect($request->get('categories'));

        return view('home', [
            'feeds' => $this->feedRepo->findByCategories($categories),
            'categories' => $this->categoryRepo->findCategoriesOrderedByName(),
            'selectedCategories' => $categories,
            'generator' => $this->generator,
        ]);
    }
}
