<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Repositories\UserRepository;
use App\Services\User\LoginService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AuthController extends Controller
{
    /**
     * @var LoginService
     */
    private $login;

    /**
     * @var UserRepository
     */
    private $userRepo;

    function __construct(LoginService $login, UserRepository $userRepo)
    {
        $this->login = $login;
        $this->userRepo = $userRepo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|RedirectResponse|View
     */
    public function showLogin()
    {
        return view('user.login');
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function submitLogin(LoginRequest $request)
    {
        if (!$this->login->validate($request->all('email', 'password'))) {
            $request->flashOnly('email');

            return view('user.login', [
                'error' => 'Invalid username or password!',
                'redirectUrl' => $request->get('redirectUrl'),
            ]);
        }

        $user = $this->userRepo->findByEmail($request->get('email'));
        $this->login->loginUser($user);

        return redirect()->route('home');
    }

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {
        $this->login->logout();

        return redirect()->route('home');
    }
}
