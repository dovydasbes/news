<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\AccountRequest;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class AccountController extends Controller
{
    /**
     * @var Hasher
     */
    private $hasher;

    /**
     * @param Hasher $hasher
     */
    function __construct(Hasher $hasher)
    {
        $this->hasher = $hasher;
    }

    /**
     * @return View
     */
    public function showAccount(): View
    {
        return view('user.account', [
            'user' => $this->user(),
        ]);
    }

    /**
     * @param AccountRequest $request
     * @return RedirectResponse
     */
    public function submitAccount(AccountRequest $request): RedirectResponse
    {
        $user = $this->user();

        $user->fill($request->all(['username', 'name']));

        if ($password = $request->get('password')) {
            $user->password = $this->hasher->make($password);
        }

        $user->saveOrFail();

        return redirect()->route('account')->with('success', 'Successfully changed account information!');
    }
}
