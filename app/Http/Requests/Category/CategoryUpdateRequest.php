<?php namespace App\Http\Requests\Category;

use App\Http\Requests\AuthorizedRequest;

class CategoryUpdateRequest extends AuthorizedRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        /** @var \App\Models\Category $category */
        $category = $this->route('category');

        return [
            'name' => 'required|string|max:255|unique:categories,name,' . $category->id,
        ];
    }
}
