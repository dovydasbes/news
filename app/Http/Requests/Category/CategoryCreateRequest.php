<?php namespace App\Http\Requests\Category;

use App\Http\Requests\AuthorizedRequest;

class CategoryCreateRequest extends AuthorizedRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255|unique:categories,name',
        ];
    }
}
