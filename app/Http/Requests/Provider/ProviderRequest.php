<?php namespace App\Http\Requests\Provider;

use App\Http\Requests\AuthorizedRequest;

class ProviderRequest extends AuthorizedRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'logo' => 'required|url',
            'url' => 'required|url',
            'rss' => 'required|url',
            'enabled' => 'sometimes|boolean',
            'category' => 'sometimes|array',
            'category.*' => 'exists:categories,id',
        ];
    }
}
