<?php namespace App\Http\Requests\User;

use App\Http\Requests\AuthorizedRequest;

class LoginRequest extends AuthorizedRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required',
            'password' => 'required|string'
        ];
    }
}
