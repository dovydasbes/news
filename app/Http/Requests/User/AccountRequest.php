<?php namespace App\Http\Requests\User;

use App\Http\Requests\AuthorizedRequest;
use App\Models\User;

class AccountRequest extends AuthorizedRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,' . $this->user()->id,
            'password' => 'sometimes|confirmed|nullable|min:' . User::MIN_PASSWORD_LENGTH,
        ];
    }
}
