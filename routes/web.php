<?php

Route::get('/', 'HomeController@showHome')->name('home');

Route::group(['middleware' => ['guest']], function () {
    Route::get('/login', 'User\AuthController@showLogin')->name('login');
    Route::post('/login', 'User\AuthController@submitLogin');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/logout', 'User\AuthController@logout')->name('logout');

    Route::get('/account', 'User\AccountController@showAccount')->name('account');
    Route::post('/account', 'User\AccountController@submitAccount');

    Route::group(['prefix' => 'providers'], function () {
        Route::get('/', 'Provider\ProviderController@showList')->name('provider:list');

        Route::get('/create', 'Provider\ProviderController@showCreate')->name('provider:create');
        Route::post('/create', 'Provider\ProviderController@submitCreate');

        Route::get('/update/{provider}', 'Provider\ProviderController@showUpdate')->name('provider:update');
        Route::post('/update/{provider}', 'Provider\ProviderController@submitUpdate');

        Route::get('/delete/{provider}', 'Provider\ProviderController@submitDelete')->name('provider:delete');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'Category\CategoryController@showList')->name('category:list');

        Route::get('/create', 'Category\CategoryController@showCreate')->name('category:create');
        Route::post('/create', 'Category\CategoryController@submitCreate');

        Route::get('/update/{category}', 'Category\CategoryController@showUpdate')->name('category:update');
        Route::post('/update/{category}', 'Category\CategoryController@submitUpdate');
    });
});
