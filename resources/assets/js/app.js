try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');
} catch (e) {}

$('#confirm-modal').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});

$('#feed-modal').on('show.bs.modal', function (event) {
    const button = $(event.relatedTarget);
    const title = button.data('title');
    const description = button.data('description');

    $(this).find('.modal-header').text(title);
    $(this).find('.modal-body').text(description ? description : title);
    $(this).find('.btn-ok').attr('href', button.data('href'));
});
