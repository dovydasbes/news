# This is News application

## Code style

The following standards are used on this project:

* PHP - [PSR-2](http://www.php-fig.org/psr/psr-2/)

## Urls

* Development url with Docker: ```news.dev```

## First run

- ```cp .env.example .env```
- ```cp docker-compose.yml.dist docker-compose.yml```
- copy ```127.0.0.1 news.dev``` to ```/etc/hosts```
- create sqlite database in ```storage/databases/news.sqlite```
- run ```composer install```
- run ```npm install```
- run database migrations ```php artisan migrate```
- compile css and js with ```npm run dev ```

## Docker

- stop local apache2/nginx services if present ```service apache2 stop```
- ```docker-compose up -d```

## Console commands

- ```artisan user:create``` - creates administrator user
- ```artisan feed:update``` - imports feeds from enabled providers
