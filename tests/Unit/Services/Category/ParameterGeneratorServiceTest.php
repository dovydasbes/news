<?php

namespace Tests\Unit;

use App\Services\Category\ParameterGeneratorService;
use Tests\TestCase;

class ParameterGeneratorServiceTest extends TestCase
{
    /**
     * @var ParameterGeneratorService
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = new ParameterGeneratorService();
    }

    public function testGenerate(): void
    {
        $categories = collect([1, 2]);

        $this->assertEquals(array_values([2]), array_values($this->service->generate($categories, 1)));
        $this->assertEquals(array_values([1, 2, 3]), array_values($this->service->generate($categories, 3)));
    }
}
