<?php

namespace Tests\Unit;

use App\Services\Validation\FeedValidatorService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class FeedValidatorServiceTest extends TestCase
{
    /**
     * @var Factory
     */
    private $factory;

    /**
     * @var FeedValidatorService
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->factory = $this->prophesize(Factory::class);

        $this->service = new FeedValidatorService($this->factory->reveal());
    }

    public function testValidateFail(): void
    {
        $validator = $this->prophesize(Validator::class);
        $validator->fails()->willReturn(true)->shouldBeCalled();

        $this->factory->make([], [
            'title' => 'required|string|max:255',
            'url' => 'required|url',
            'description' => 'sometimes',
            'published' => 'required|date',
            'guid' => 'required|string',
        ])->willReturn($validator->reveal())->shouldBeCalled();

        $this->expectException(ValidationException::class);

        $this->service->validateFeed([]);
    }

    public function testValidatePass(): void
    {
        $validator = $this->prophesize(Validator::class);
        $validator->fails()->willReturn(false)->shouldBeCalled();

        $this->factory->make([], [
            'title' => 'required|string|max:255',
            'url' => 'required|url',
            'description' => 'sometimes',
            'published' => 'required|date',
            'guid' => 'required|string',
        ])->willReturn($validator->reveal())->shouldBeCalled();

        $this->service->validateFeed([]);
    }
}
