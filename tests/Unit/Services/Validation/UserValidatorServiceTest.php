<?php

namespace Tests\Unit;

use App\Services\Validation\UserValidatorService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class UserValidatorServiceTest extends TestCase
{
    /**
     * @var Factory
     */
    private $factory;

    /**
     * @var UserValidatorService
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->factory = $this->prophesize(Factory::class);

        $this->service = new UserValidatorService($this->factory->reveal());
    }

    public function testValidateFail(): void
    {
        $validator = $this->prophesize(Validator::class);
        $validator->fails()->willReturn(true)->shouldBeCalled();

        $this->factory->make([], [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|string|min:6',
        ])->willReturn($validator->reveal())->shouldBeCalled();

        $this->expectException(ValidationException::class);

        $this->service->validateUser([]);
    }

    public function testValidatePass(): void
    {
        $validator = $this->prophesize(Validator::class);
        $validator->fails()->willReturn(false)->shouldBeCalled();

        $this->factory->make([], [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|string|min:6',
        ])->willReturn($validator->reveal())->shouldBeCalled();

        $this->service->validateUser([]);
    }
}
