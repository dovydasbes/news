<?php

namespace Tests\Unit;

use App\Models\User;
use App\Services\User\LoginService;
use Illuminate\Auth\AuthManager;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Auth\StatefulGuard;
use Tests\TestCase;

class LoginServiceTest extends TestCase
{
    /**
     * @var StatefulGuard
     */
    private $auth;

    /**
     * @var Repository
     */
    private $config;

    /**
     * @var LoginService
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->auth = $this->prophesize(StatefulGuard::class);
        $this->config = $this->prophesize(Repository::class);

        $authManager = $this->prophesize(AuthManager::class);
        $authManager->guard()->willReturn($this->auth->reveal());

        $this->service = new LoginService($authManager->reveal(), $this->config->reveal());
    }

    public function testLoginUser(): void
    {
        $user = $this->prophesize(User::class);

        $this->auth->login($user->reveal())->shouldBeCalled();

        $this->service->loginUser($user->reveal());
    }

    public function testLogoutUser(): void
    {
        $this->auth->logout()->shouldBeCalled();

        $this->service->logout();
    }

    public function testValidateTrue(): void
    {
        $this->auth->validate([])->shouldBeCalled()->willReturn(true);

        $this->assertTrue($this->service->validate([]));
    }


    public function testValidateFalse(): void
    {
        $this->auth->validate([])->shouldBeCalled()->willReturn(false);

        $this->assertFalse($this->service->validate([]));
    }
}
