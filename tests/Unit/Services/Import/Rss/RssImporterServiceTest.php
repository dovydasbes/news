<?php

namespace Tests\Unit;

use App\Models\Feed;
use App\Models\Provider;
use App\Repositories\FeedRepository;
use App\Services\Import\Exception\ImportException;
use App\Services\Import\Rss\RssImporterService;
use App\Services\Validation\FeedValidatorService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prophecy\Argument;
use Tests\TestCase;

class RssImporterServiceTest extends TestCase
{
    /**
     * @var Feed
     */
    private $feed;

    /**
     * @var FeedRepository
     */
    private $feedRepo;

    /**
     * @var FeedValidatorService
     */
    private $validator;

    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var RssImporterService
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->feed = $this->prophesize(Feed::class);
        $this->feedRepo = $this->prophesize(FeedRepository::class);
        $this->validator = $this->prophesize(FeedValidatorService::class);
        $this->provider = $this->prophesize(Provider::class);

        $this->service = new RssImporterService(
            $this->feed->reveal(),
            $this->feedRepo->reveal(),
            $this->validator->reveal()
        );
    }

    public function testImportCorruptedXML(): void
    {
        $this->provider->getAttribute('rss')->willReturn(__DIR__ . '/absent.xml')->shouldBeCalled();

        $this->expectException(ImportException::class);
        $this->expectExceptionMessage("XML is corrupted");

        $this->service->import($this->provider->reveal());
    }

    public function testImportInvalidXML(): void
    {
        $this->provider->getAttribute('rss')->willReturn(__DIR__ . '/invalid.xml')->shouldBeCalled();

        $this->expectException(ImportException::class);
        $this->expectExceptionMessage("XML format is invalid");

        $this->service->import($this->provider->reveal());
    }

    public function testImportInvalidFeed(): void
    {
        $this->provider->getAttribute('rss')->willReturn(__DIR__ . '/import.xml')->shouldBeCalled();

        $this->validator->validateFeed(Argument::type('array'))
            ->willThrow(ImportException::invalidFeed())
            ->shouldBeCalled();

        $this->expectException(ImportException::class);
        $this->expectExceptionMessage("XML has invalid feed items");

        $this->service->import($this->provider->reveal());
    }

    public function testImportUpdate(): void
    {
        $date = Carbon::parse('Tue, 21 Nov 2017 22:33:51 +0200')->setTimezone('UTC');

        $feed = $this->prophesize(Feed::class);
        $feed->fill([
            'title' => 'Test item',
            'description' => '',
            'url' => 'www.test.com',
            'guid' => 'abc123',
            'published' => $date,
        ])->shouldBeCalled();
        $feed->saveOrFail()->shouldBeCalled();

        $this->provider->getAttribute('rss')->willReturn(__DIR__ . '/import.xml')->shouldBeCalled();

        $this->validator->validateFeed(Argument::type('array'))->shouldBeCalled();

        $this->feedRepo->findByProviderAndGuid($this->provider->reveal(), 'abc123')
            ->willReturn($feed->reveal())
            ->shouldBeCalled();

        $this->service->import($this->provider->reveal());
    }

    public function testImportCreate(): void
    {
        $date = Carbon::parse('Tue, 21 Nov 2017 22:33:51 +0200')->setTimezone('UTC');

        $belongsTo = $this->prophesize(BelongsTo::class);
        $belongsTo->associate($this->provider->reveal())->shouldBeCalled();

        $feed = $this->prophesize(Feed::class);
        $feed->provider()->willReturn($belongsTo->reveal())->shouldBeCalled();
        $feed->saveOrFail()->shouldBeCalled();

        $this->feed->newInstance([
            'title' => 'Test item',
            'description' => '',
            'url' => 'www.test.com',
            'guid' => 'abc123',
            'published' => $date,
        ])->willReturn($feed->reveal())->shouldBeCalled();

        $this->provider->getAttribute('rss')->willReturn(__DIR__ . '/import.xml')->shouldBeCalled();

        $this->validator->validateFeed(Argument::type('array'))->shouldBeCalled();

        $this->feedRepo->findByProviderAndGuid($this->provider->reveal(), 'abc123')->willReturn(null)->shouldBeCalled();

        $this->service->import($this->provider->reveal());
    }
}
