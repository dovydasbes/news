<?php

namespace Tests\Unit;

use App\Services\Import\Exception\ImportException;
use Tests\TestCase;

class ImportExceptionTest extends TestCase
{
    public function testCorruptedXML(): void
    {
        $this->assertEquals(ImportException::corruptedXML()->getMessage(), 'XML is corrupted');
    }

    public function testInvalidXML(): void
    {
        $this->assertEquals(ImportException::invalidXML()->getMessage(), 'XML format is invalid');
    }

    public function testInvalidFeed(): void
    {
        $this->assertEquals(ImportException::invalidFeed()->getMessage(), 'XML has invalid feed items');
    }
}
