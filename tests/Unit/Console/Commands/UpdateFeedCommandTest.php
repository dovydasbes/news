<?php

namespace Tests\Unit;

use App\Console\Commands\UpdateFeed;
use App\Models\Provider;
use App\Repositories\ProviderRepository;
use App\Services\Import\Exception\ImportException;
use App\Services\Import\Rss\RssImporterService;
use Illuminate\Database\Eloquent\Collection;
use Prophecy\Argument;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\TestCase;

class UpdateFeedCommandTest extends TestCase
{
    /**
     * @var ProviderRepository
     */
    private $providerRepo;

    /**
     * @var RssImporterService
     */
    private $rss;

    /**
     * @var CommandTester
     */
    private $tester;

    protected function setUp(): void
    {
        parent::setUp();

        $this->providerRepo = $this->prophesize(ProviderRepository::class);
        $this->rss = $this->prophesize(RssImporterService::class);

        $command = new UpdateFeed($this->providerRepo->reveal(), $this->rss->reveal());

        $command->setLaravel(app());

        $this->tester = new CommandTester($command);
    }

    public function testHandleEmptyProviders(): void
    {
        $this->providerRepo->findEnabled()->willReturn(new Collection())->shouldBeCalled();

        $this->rss->import(Argument::any())->shouldNotBeCalled();

        $this->tester->execute([]);
    }

    public function testHandleImportException(): void
    {
        $provider = $this->prophesize(Provider::class);
        $provider->getAttribute('name')->willReturn('Test')->shouldBeCalled();

        $this->providerRepo->findEnabled()->willReturn(new Collection([$provider->reveal()]))->shouldBeCalled();

        $this->rss->import($provider->reveal())->willThrow(ImportException::invalidFeed())->shouldBeCalled();

        $this->tester->execute([]);
    }

    public function testHandleImport(): void
    {
        $provider = $this->prophesize(Provider::class);
        $provider->getAttribute('name')->willReturn('Test')->shouldBeCalled();

        $this->providerRepo->findEnabled()->willReturn(new Collection([$provider->reveal()]))->shouldBeCalled();

        $this->rss->import($provider->reveal())->shouldBeCalled();

        $this->tester->execute([]);
    }
}
