<?php

namespace Tests\Unit;

use App\Console\Commands\CreateUser;
use App\Models\User;
use App\Services\Validation\UserValidatorService;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Validation\ValidationException;
use Prophecy\Argument;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\TestCase;

class CreateUserCommandTest extends TestCase
{
    /**
     * @var UserValidatorService
     */
    private $validator;

    /**
     * @var Hasher
     */
    private $hasher;

    /**
     * @var User
     */
    private $user;

    /**
     * @var CommandTester
     */
    private $tester;

    protected function setUp(): void
    {
        parent::setUp();

        $this->validator = $this->prophesize(UserValidatorService::class);
        $this->hasher = $this->prophesize(Hasher::class);
        $this->user = $this->prophesize(User::class);

        $command = new CreateUser($this->validator->reveal(), $this->hasher->reveal(), $this->user->reveal());

        $command->setLaravel(app());

        $this->tester = new CommandTester($command);
    }

    public function testCreateUserException(): void
    {
        $this->tester->setInputs(["Test", "test@test.com", "password", "password"]);

        $this->validator->validateUser([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ])->willThrow(ValidationException::withMessages(['test']))->shouldBeCalled();

        $this->user->newInstance(Argument::any())->shouldNotBeCalled();

        $this->tester->execute([]);
    }

    public function testCreateUser(): void
    {
        $this->tester->setInputs(["Test", "test@test.com", "password", "password"]);

        $this->validator->validateUser([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => 'password',
            'password_confirmation' => 'password',
        ])->shouldBeCalled();

        $this->hasher->make('password')->willReturn('s3cretpassword')->shouldBeCalled();

        $this->user->saveOrFail()->shouldBeCalled();
        $this->user->newInstance([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => 's3cretpassword',
        ])->willReturn($this->user->reveal())->shouldBeCalled();

        $this->tester->execute([]);
    }
}
